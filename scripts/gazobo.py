#!/usr/bin/python3
import subprocess
import time
import math
import threading
import sys
import rclpy
import asyncio
import cv2
import ros2node
from cv_bridge import CvBridge
import numpy as np
import PyQt5
import ros2topic
from rclpy.node import Node
from std_msgs.msg import Float64MultiArray
from geometry_msgs.msg import Twist
from sensor_msgs.msg import Image
from sensor_msgs.msg import LaserScan, Imu
from geometry_msgs.msg import Quaternion
from tf2_msgs.msg import TFMessage
from turtlesim.msg import Color
from PyQt5.QtCore import Qt, QThread, pyqtSignal
import tf2_ros
from PyQt5.QtGui import QImage, QPixmap
import numpy

import tf2_py

def euler_from_quaternion(quaternion):
    x = quaternion.x
    y = quaternion.y
    z = quaternion.z
    w = quaternion.w

    sinr_cosp = 2 * (w * x + y * z)
    cosr_cosp = 1 - 2 * (x * x + y * y)
    roll = np.arctan2(sinr_cosp, cosr_cosp)

    sinp = 2 * (w * y - z * x)
    pitch = np.arcsin(sinp)

    siny_cosp = 2 * (w * z + x * y)
    cosy_cosp = 1 - 2 * (y * y + z * z)
    yaw = np.arctan2(siny_cosp, cosy_cosp)

    return roll, pitch, yaw

class Sensor():
    def __init__(self):
        self.type = ''
        self.ranges = []

class Commander(Node):
    def __init__(self):
        super().__init__('commander')
        self.pub_vel = self.create_publisher(Float64MultiArray, '/wheels_controller/commands', 10)
        msg = Float64MultiArray()
        msg.data = [.0, .0, .0, .0]
        self.pub_vel.publish(msg)
        self.pub_vel.wait_for_all_acked()
        time.sleep(3)
        self.tf = None
        self.create_subscription(LaserScan, '/lf/lidar', self.lf_sensor_callback, 10)
        self.odom = self.create_subscription(Imu, '/odom/imu', self.get_tf, 10)
        self.create_subscription(LaserScan, '/ls/lidar', self.ls_sensor_callback, 10)
        self.create_subscription(LaserScan, '/rf/lidar', self.rf_sensor_callback, 10)
        self.create_subscription(LaserScan, '/rs/lidar', self.rs_sensor_callback, 10)
        self.create_subscription(LaserScan, '/rl/lidar', self.lr_sensor_callback, 10)
        self.create_subscription(LaserScan, '/rr/lidar', self.rr_sensor_callback, 10)
        self.sensors = {'lf': Sensor(), 'rf': Sensor(), 'ls': Sensor(), 'rs': Sensor(), 'lr': Sensor(), 'rr': Sensor()}

        #self.timer = self.create_timer(1, self.timer_callback)
        self.bridge = CvBridge()
    
    def lf_sensor_callback(self, msg):
        if len(msg.ranges) == 3:
            self.sensors['lf'].type = 'ir'
            if min(msg.ranges) < 12:
                self.sensors['lf'].ranges = [True]
            else:
                self.sensors['lf'].ranges = [False]
        elif len(msg.ranges) == 2:
            self.sensors['lf'].type = 'us'
            self.sensors['lf'].ranges = [min(msg.ranges)]
        else:
            self.sensors['lf'].type = 'lidar'
            self.sensors['lf'].ranges = msg.ranges

    def ls_sensor_callback(self, msg):
        if len(msg.ranges) == 3:
            self.sensors['ls'].type = 'ir'
            if min(msg.ranges) < 12:
                self.sensors['ls'].ranges = [True]
            else:
                self.sensors['ls'].ranges = [False]
        elif len(msg.ranges) == 2:
            self.sensors['ls'].type = 'us'
            self.sensors['ls'].ranges = [min(msg.ranges)]
        else:
            self.sensors['ls'].type = 'lidar'
            self.sensors['ls'].ranges = msg.ranges
    
    def rf_sensor_callback(self, msg):
        if len(msg.ranges) == 3:
            self.sensors['rf'].type = 'ir'
            if min(msg.ranges) < 12:
                self.sensors['rf'].ranges = [True]
            else:
                self.sensors['rf'].ranges = [False]
        elif len(msg.ranges) == 2:
            self.sensors['rf'].type = 'us'
            self.sensors['rf'].ranges = [min(msg.ranges)]
        else:
            self.sensors['rf'].type = 'lidar'
            self.sensors['rf'].ranges = msg.ranges

    def rs_sensor_callback(self, msg):
        if len(msg.ranges) == 3:
            self.sensors['rs'].type = 'ir'
            if min(msg.ranges) < 12:
                self.sensors['rs'].ranges = [True]
            else:
                self.sensors['rs'].ranges = [False]
        elif len(msg.ranges) == 2:
            self.sensors['rs'].type = 'us'
            self.sensors['rs'].ranges = [min(msg.ranges)]
        else:
            self.sensors['rs'].type = 'lidar'
            self.sensors['rs'].ranges = msg.ranges

    def lr_sensor_callback(self, msg):
        if len(msg.ranges) == 3:
            self.sensors['lr'].type = 'ir'
            if min(msg.ranges) < 12:
                self.sensors['lr'].ranges = [True]
            else:
                self.sensors['lr'].ranges = [False]
        elif len(msg.ranges) == 2:
            self.sensors['lr'].type = 'us'
            self.sensors['lr'].ranges = [min(msg.ranges)]
        else:
            self.sensors['lr'].type = 'lidar'
            self.sensors['lr'].ranges = msg.ranges

    def get_tf(self, msg):
        self.tf = msg.orientation


    def rr_sensor_callback(self, msg):
        if len(msg.ranges) == 3:
            self.sensors['rr'].type = 'rr'
            if min(msg.ranges) < 12:
                self.sensors['rr'].ranges = [True]
            else:
                self.sensors['rr'].ranges = [False]
        elif len(msg.ranges) == 2:
            self.sensors['rr'].type = 'us'
            self.sensors['rr'].ranges = [min(msg.ranges)]
        else:
            self.sensors['rr'].type = 'lidar'
            self.sensors['rr'].ranges = msg.ranges
        
import os
class Robot():
    def __init__(self):
        dirname, _ = os.path.split(os.path.abspath(sys.argv[0]))
        subprocess.Popen(["python3", os.path.join(dirname, "GUI.py")])
        rclpy.init(args=None)
        self.commander = Commander()
        executor = rclpy.executors.MultiThreadedExecutor()
        executor.add_node(self.commander)
        self.executor_thread = threading.Thread(target=executor.spin, daemon=True)
        self.executor_thread.start()
        rate = self.commander.create_rate(2)
        while not rclpy.ok():
            time.sleep(0.5)
        time.sleep(2)

    def wheels_speed(self, speed_front_right, speed_front_left, speed_back_right, speed_back_left):
        while not rclpy.ok():
            time.sleep(0.5)
        msg = Float64MultiArray()
        msg.data = [-speed_front_right, -speed_front_left, -speed_back_right, -speed_back_left]
        self.commander.pub_vel.publish(msg)
    
    def get_sensors(self):
        return self.commander.sensors

    def move_back(self, speed):
        while not rclpy.ok():
            time.sleep(0.5)
        msg = Float64MultiArray()
        msg.data = [speed, speed, speed, speed]
        self.commander.pub_vel.publish(msg)
    
    def move_forward(self, speed):
        while not rclpy.ok():
            time.sleep(0.5)
        msg = Float64MultiArray()
        msg.data = [-speed, -speed, -speed, -speed]
        self.commander.pub_vel.publish(msg)

    def stop(self):
        while not rclpy.ok():
            time.sleep(0.5)
        msg = Float64MultiArray()
        msg.data = [.0, .0, .0, .0]
        self.commander.pub_vel.publish(msg)

    def get_z_angle(self):
        Q = self.commander.tf
        if Q != None: 
            return euler_from_quaternion(Q)[2]
        return None
    
    def spin_left_for_angle(self, angle, speed=3.0):
        start = self.get_z_angle()
        if start < 0:
            start = np.pi*2 + start 
        end = (start + angle) % (2*np.pi)
        self.spin_left(speed)
        prev = start
        while True:
            curr = self.get_z_angle()
            if curr < 0:
                curr = np.pi*2 + curr 
            if curr < prev:
                if end <= curr or end >= prev:
                    self.stop()
                    break
            if prev <= end <= curr:
                self.stop()
                break
            prev = curr
    
    def spin_right_for_angle(self, angle, speed=3.0):
        start = self.get_z_angle()
        if start < 0:
            start = np.pi*2 + start 
        end = (start - angle + 2*np.pi) % (2*np.pi)
        self.spin_right(speed)
        prev = start
        while True:
            curr = self.get_z_angle()
            if curr < 0:
                curr = np.pi*2 + curr 
            if prev < curr:
                if end >= curr or end <= prev:
                    self.stop()
                    break
            if curr <= end <= prev:
                self.stop()
                break
            prev = curr
        

    def spin_left(self, speed):
        while not rclpy.ok():
            time.sleep(0.5)
        msg = Float64MultiArray()
        msg.data = [-speed, -speed, speed, speed]
        self.commander.pub_vel.publish(msg)

    def spin_right(self, speed):
        while not rclpy.ok():
            time.sleep(0.5)
        msg = Float64MultiArray()
        msg.data = [speed, speed, -speed, -speed]
        self.commander.pub_vel.publish(msg)

    def close(self):
        self.commander.destroy_publisher(self.commander.pub_vel)
        self.commander.destroy_subscription(self.commander.odom)
        rclpy.shutdown()
        self.executor_thread.join()