
#!/usr/bin/python3
import subprocess
import time
import math
import threading
import sys
import rclpy
import asyncio
import cv2
from cv_bridge import CvBridge
import numpy as np
import PyQt5
from rclpy.node import Node
from std_msgs.msg import Float64MultiArray
from geometry_msgs.msg import Twist
from sensor_msgs.msg import Image
from sensor_msgs.msg import LaserScan
from turtlesim.msg import Color
from PyQt5.QtCore import Qt, QThread, pyqtSignal
from PyQt5.QtGui import QImage, QPixmap
from PyQt5.QtWidgets import (
    QApplication,
    QCheckBox,
    QComboBox,
    QDateEdit,
    QDateTimeEdit,
    QDial,
    QDoubleSpinBox,
    QFontComboBox,
    QLabel,
    QHBoxLayout,
    QLCDNumber,
    QLineEdit,
    QMainWindow,
    QProgressBar,
    QPushButton,
    QRadioButton,
    QSlider,
    QSpinBox,
    QTimeEdit,
    QVBoxLayout,
    QWidget,
)


vel = 0.0

class Sensor():
    def __init__(self):
        self.type = ''
        self.ranges = []

class MainWindow(QMainWindow):
    def __init__(self):
        super().__init__()
        global stream_label, stream_label_2, sensor_label, sensor_label_2, sensor_label_3, sensor_label_4, sensor_label_5, sensor_label_6
        self.setWindowTitle("Widgets App")
        hlayout = QHBoxLayout()
        stream_label = QLabel()
        stream_label_2 = QLabel()
        sensor_label = QLabel()
        sensor_label_2 = QLabel()
        sensor_label_3 = QLabel()
        sensor_label_4 = QLabel()
        sensor_label_5 = QLabel()
        sensor_label_6 = QLabel()
        layout = QVBoxLayout()
        layout_2 = QVBoxLayout()
        layout.addWidget(stream_label)
        layout.addWidget(stream_label_2)
        layout_2.addWidget(sensor_label)
        layout_2.addWidget(sensor_label_2)
        layout_2.addWidget(sensor_label_3)
        layout_2.addWidget(sensor_label_4)
        layout_2.addWidget(sensor_label_5)
        layout_2.addWidget(sensor_label_6)
        hlayout.addLayout(layout)
        hlayout.addLayout(layout_2)

        widget = QWidget()
        widget.setLayout(hlayout)
        self.setCentralWidget(widget)


class Commander(Node):
    def __init__(self):
        time.sleep(1)
        super().__init__('gui')
        self.camera_sub = self.create_subscription(Image, '/rear_camera/image_raw', self.sensor_callback, 10)
        self.camera_sub_front = self.create_subscription(Image, '/front_camera/image_raw', self.front_camera_callback, 10)
        self.sub1 = self.create_subscription(LaserScan, '/lf/lidar', self.lf_sensor_callback, 10)
        self.sub2 = self.create_subscription(LaserScan, '/ls/lidar', self.ls_sensor_callback, 10)
        self.sub3 = self.create_subscription(LaserScan, '/rf/lidar', self.rf_sensor_callback, 10)
        self.sub4 = self.create_subscription(LaserScan, '/rs/lidar', self.rs_sensor_callback, 10)
        self.sub5 = self.create_subscription(LaserScan, '/rl/lidar', self.lr_sensor_callback, 10)
        self.sub6 = self.create_subscription(LaserScan, '/rr/lidar', self.rr_sensor_callback, 10)
        self.sensors = {'lf': Sensor(), 'rf': Sensor(), 'ls': Sensor(), 'rs': Sensor(), 'lr': Sensor(), 'rr': Sensor()}

        #self.timer = self.create_timer(1, self.timer_callback)
        self.bridge = CvBridge()
        

    def lf_sensor_callback(self, msg):
        global sensor_label
        total = 'Левый передний: \n['
        if len(msg.ranges) == 3:
            self.sensors['lf'].type = 'ir'
            if min(msg.ranges) < 12:
                total += "есть препятствие"
                self.sensors['lf'].ranges = [True]
            else:
                total += "нет препятствий"
                self.sensors['lf'].ranges = [False]
        elif len(msg.ranges) == 2:
            self.sensors['lf'].type = 'us'
            self.sensors['lf'].ranges = [min(msg.ranges)]
            total += '{:.2f}'.format(min(msg.ranges))
        else:
            self.sensors['lf'].type = 'lidar'
            self.sensors['lf'].ranges = msg.ranges
            lst = ['{:.2f}'.format(x) for x in msg.ranges]
            for x in lst:
                total += x + ' '
        total += ']'
        sensor_label.setText(total)

    def ls_sensor_callback(self, msg):
        global sensor_label_2
        total = 'Левый боковой: \n['
        if len(msg.ranges) == 3:
            self.sensors['ls'].type = 'ir'
            if min(msg.ranges) < 12:
                total += "есть препятствие"
                self.sensors['ls'].ranges = [True]
            else:
                total += "нет препятствий"
                self.sensors['ls'].ranges = [False]
        elif len(msg.ranges) == 2:
            self.sensors['ls'].type = 'us'
            self.sensors['ls'].ranges = [min(msg.ranges)]
            total += '{:.2f}'.format(min(msg.ranges))
        else:
            self.sensors['ls'].type = 'lidar'
            self.sensors['ls'].ranges = msg.ranges
            lst = ['{:.2f}'.format(x) for x in msg.ranges]
            for x in lst:
                total += x + ' '
        total += ']'
        sensor_label_2.setText(total)
    
    def rf_sensor_callback(self, msg):
        global sensor_label_3
        total = 'Правый передний: \n['
        if len(msg.ranges) == 3:
            self.sensors['rf'].type = 'ir'
            if min(msg.ranges) < 12:
                total += "есть препятствие"
                self.sensors['rf'].ranges = [True]
            else:
                total += "нет препятствий"
                self.sensors['rf'].ranges = [False]
        elif len(msg.ranges) == 2:
            self.sensors['rf'].type = 'us'
            self.sensors['rf'].ranges = [min(msg.ranges)]
            total += '{:.2f}'.format(min(msg.ranges))
        else:
            self.sensors['rf'].type = 'lidar'
            self.sensors['rf'].ranges = msg.ranges
            lst = ['{:.2f}'.format(x) for x in msg.ranges]
            for x in lst:
                total += x + ' '
        total += ']'
        sensor_label_3.setText(total)

    def rs_sensor_callback(self, msg):
        global sensor_label_4
        total = 'Правый боковой: \n['
        if len(msg.ranges) == 3:
            self.sensors['rs'].type = 'ir'
            if min(msg.ranges) < 12:
                total += "есть препятствие"
                self.sensors['rs'].ranges = [True]
            else:
                total += "нет препятствий"
                self.sensors['rs'].ranges = [False]
        elif len(msg.ranges) == 2:
            self.sensors['rs'].type = 'us'
            self.sensors['rs'].ranges = [min(msg.ranges)]
            total += '{:.2f}'.format(min(msg.ranges))
        else:
            self.sensors['rs'].type = 'lidar'
            self.sensors['rs'].ranges = msg.ranges
            lst = ['{:.2f}'.format(x) for x in msg.ranges]
            for x in lst:
                total += x + ' '
        total += ']'
        sensor_label_4.setText(total)

    def lr_sensor_callback(self, msg):
        global sensor_label_5
        total = 'Левый задний: \n['
        if len(msg.ranges) == 3:
            self.sensors['lr'].type = 'ir'
            if min(msg.ranges) < 12:
                total += "есть препятствие"
                self.sensors['lr'].ranges = [True]
            else:
                total += "нет препятствий"
                self.sensors['lr'].ranges = [False]
        elif len(msg.ranges) == 2:
            self.sensors['lr'].type = 'us'
            self.sensors['lr'].ranges = [min(msg.ranges)]
            total += '{:.2f}'.format(min(msg.ranges))
        else:
            self.sensors['lr'].type = 'lidar'
            self.sensors['lr'].ranges = msg.ranges
            lst = ['{:.2f}'.format(x) for x in msg.ranges]
            for x in lst:
                total += x + ' '
        total += ']'
        sensor_label_5.setText(total)

    def rr_sensor_callback(self, msg):
        global sensor_label_6
        total = 'Правый задний: \n['
        if len(msg.ranges) == 3:
            self.sensors['rr'].type = 'ir'
            if min(msg.ranges) < 12:
                total += "есть препятствие"
                self.sensors['rr'].ranges = [True]
            else:
                total += "нет препятствий"
                self.sensors['rr'].ranges = [False]
        elif len(msg.ranges) == 2:
            self.sensors['rr'].type = 'us'
            self.sensors['rr'].ranges = [min(msg.ranges)]
            total += '{:.2f}'.format(min(msg.ranges))
        else:
            self.sensors['rr'].type = 'lidar'
            self.sensors['rr'].ranges = msg.ranges
            lst = ['{:.2f}'.format(x) for x in msg.ranges]
            for x in lst:
                total += x + ' '
        total += ']'
        sensor_label_6.setText(total)



    def sensor_callback(self, msg):
        global stream_label
        img = self.bridge.imgmsg_to_cv2(msg, 'bgr8')
        h, w, c = img.shape
        b = 3 * w
        img = QImage(img.data, w, h, b, QImage.Format_RGB888).rgbSwapped()
        stream_label.setPixmap(QPixmap.fromImage(img))


    def front_camera_callback(self, msg):
        global stream_label_2
        img = self.bridge.imgmsg_to_cv2(msg, 'bgr8')
        h, w, c = img.shape
        b = 3 * w
        img = QImage(img.data, w, h, b, QImage.Format_RGB888).rgbSwapped()
        stream_label_2.setPixmap(QPixmap.fromImage(img))


    def timer_callback(self):
        global vel
        print(vel)
        msg = Twist()
        msg.linear.x = vel
        self.pub_vel.publish(msg)

        # self.app = QApplication(sys.argv)
        # self.window = MainWindow()
        # self.window.show()
        # self.app.exec()


app = QApplication(sys.argv)
window = MainWindow()
window.show()
rclpy.init(args=None)
commander = Commander()
executor = rclpy.executors.MultiThreadedExecutor()
executor.add_node(commander)
executor_thread = threading.Thread(target=executor.spin, daemon=True)
executor_thread.start()
app.exec()
commander.destroy_subscription(commander.sub1)
commander.destroy_subscription(commander.sub2)
commander.destroy_subscription(commander.sub3)
commander.destroy_subscription(commander.sub4)
commander.destroy_subscription(commander.sub5)
commander.destroy_subscription(commander.sub6)
rclpy.shutdown()
executor_thread.join()

