from gazobo import Robot
import time
import numpy as np
import random

robot = Robot() #переменная, по которой мы задаем команды роботу
robot.stop() #останавливаем робота на всякий случай
time.sleep(1) #добавим задержку для того, чтобы Gazebo успело загрузиться
sensors = robot.get_sensors() #создаем переменную, по которой мы можем обращаться к датчикам

speed = 20.0 #скорость (only float)
dist = 1.0 #дистанция до перпятсвия, которая будет сравниваться с показаниями датчиков (only float)

while True: #бесконечный цикл
    if sensors['lf'].ranges[0] > dist and sensors['rf'].ranges[0] > dist: #если левый передний и правый передний датчики дают показания больше, чем переменная dist...
        robot.move_forward(speed) #...робот едет вперед с заданной скоростью speed
        continue
    if random.randint(1, 2) == 1: # робот наугад выбирает, в какую сторону ему повернуться в случае нахождения препятсвия перед собой (не выполняется условие растояние_передние_датчики > dist) и заходит в следующие условия:
        while sensors['lf'].ranges[0] < dist and sensors['rf'].ranges[0] < dist: #пока дистанция до препятсвия не будет удовлетворительной....
            robot.spin_left(speed=speed)#...робот поворачивается налево со скоростью speed
    else:
        while sensors['lf'].ranges[0] < dist and sensors['rf'].ranges[0] < dist: #по аналогии с предыдущим условием
            robot.spin_right(speed=speed) #поворачиваемся направо
    
robot.close() #удаляем все подписки на датчики, чтобы не переполнять ничего лишнего
